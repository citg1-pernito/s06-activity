

-- 2.a.	List the books Authored by Marjorie Green
SELECT 
	title.title AS title, 
	author.au_lname AS au_lname, 
	author.au_fname AS au_fname 
FROM title 
INNER JOIN author_title 
	ON title.title_id = author_title.title_id 
INNER JOIN author 
	ON author_title.au_id = author.au_id 
WHERE 
	author.au_lname = 'Green' 
AND 
	author.au_fname = 'Marjorie';


-- 2.b.	List the books Authored by Michael O'Leary
SELECT 
	title.title AS title, 
	author.au_lname AS au_lname, 
	author.au_fname AS au_fname 
FROM title 
INNER JOIN author_title 
	ON title.title_id = author_title.title_id 
INNER JOIN author 
	ON author_title.au_id = author.au_id 
WHERE 
	author.au_fname = 'Michael';


-- 2.c.	Write the author/s of "The Busy Executives Database Guide"
SELECT 
	title.title AS title, 
	author.au_lname AS au_lname, 
	author.au_fname AS au_fname 
FROM title 
INNER JOIN author_title 
	ON title.title_id = author_title.title_id 
INNER JOIN author 
	ON author_title.au_id = author.au_id 
WHERE 
	title.title_id = 'BU1032';


-- 2.d.	Identify the publisher of "But Is It User Friendly?"
SELECT 
	title.title AS title, 
	publisher.pub_name AS pub_name 
FROM title 
INNER JOIN publisher 
	ON title.pub_id = publisher.pub_id 
WHERE 
	title.title_id = 'PC1035';


-- 2.e.	List the books published by Algodata Infosystems
SELECT 
	title.title AS title, 
	publisher.pub_name AS pub_name 
FROM title 
INNER JOIN publisher 
	ON title.pub_id = publisher.pub_id 
WHERE 
	publisher.pub_id = 1389;


-- 3.		Create SQL Syntax and Queries to create a database based on the ERD
CREATE DATABASE blog_db;

USE blog_db;

CREATE TABLE users(
id INT NOT NULL AUTO_INCREMENT, 
email VARCHAR(100) NOT NULL, 
password VARCHAR(300) NOT NULL, 
datetime_created DATETIME NOT NULL, 
PRIMARY KEY (id)
);

CREATE TABLE posts(
id INT NOT NULL AUTO_INCREMENT, 
author_id INT NOT NULL, 
title VARCHAR(500) NOT NULL, 
content VARCHAR(5000), 
datetime_posted DATETIME NOT NULL, 
PRIMARY KEY (id), 
FOREIGN KEY (author_id) REFERENCES users(id)
);

CREATE TABLE post_likes(
id INT NOT NULL AUTO_INCREMENT, 
post_id INT NOT NULL, 
user_id INT NOT NULL, 
datetime_liked DATETIME NOT NULL, 
PRIMARY KEY (id), 
FOREIGN KEY (post_id) REFERENCES posts(id), 
FOREIGN KEY (user_id) REFERENCES users(id)
);

CREATE TABLE post_comments(
id INT NOT NULL AUTO_INCREMENT, 
post_id INT NOT NULL, 
user_id INT NOT NULL, 
content VARCHAR(5000), 
datetime_commented DATETIME NOT NULL, 
PRIMARY KEY (id), 
FOREIGN KEY (post_id) REFERENCES posts(id), 
FOREIGN KEY (user_id) REFERENCES users(id)
);
